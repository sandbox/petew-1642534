// Provides the autocomplete extensions to enable record IDs to be captured correctly.
// It plays nicely with the normal autocomplete functions and still calls them.
// Though this does mean if things drastically change in the autocomplete code we might need to make some updates.
(function ($) {

// Override the select method
Drupal.jsAC.prototype.selectOrig = Drupal.jsAC.prototype.select;
Drupal.jsAC.prototype.select = function (node) {
  if(this.autocomplete_with_id || this.idfield) {
    // We still give the original select method a chance to do it's stuff.
    this.selectOrig(node);
    // We also update the hidden field which contains the real ID for the selected item.
    $(this.idfield).val( $(node).data('autocompleteID') );
    // Update the current input value into the id field so we can 'undo' things if needed.
    $(this.idfield).data('inputValue', this.input.value );
  } else {
    // This isn't using our enhanced method, call the original handler.
    this.selectOrig(node);
  }
};

// Override the hidePopup method
Drupal.jsAC.prototype.hidePopupOrig = Drupal.jsAC.prototype.hidePopup;
Drupal.jsAC.prototype.hidePopup = function (keycode) {
  if(this.autocomplete_with_id || this.idfield) {
    // this.selected is destroyed by hidePopup so we record what we need here.
    var userselect = this.selected;
    // We still call the original hidePopup method so it can do it's stuff.
    this.hidePopupOrig(keycode);
    
    if(userselect) {
      // We update the hidden field to contain the real ID for the selected item.
      $(this.idfield).val( $(userselect).data('autocompleteID') );
      // Update the current input value into the id field so we can revert/undo things if needed.
      $(this.idfield).data('inputValue', this.input.value );
    }
  } else {
    // This isn't using our enhanced method, call the original handler.
    this.hidePopupOrig(keycode);
  }
};

// Override the found method
Drupal.jsAC.prototype.foundOrig = Drupal.jsAC.prototype.found;
Drupal.jsAC.prototype.found = function (matches) {
  // Perform our setup if we haven't already
  if(!this.autocomplete_setup_completed) {
    this.setupIdField();
    this.autocomplete_setup_completed = true;
  }
  if(matches["#autocomplete_with_id"] || this.autocomplete_with_id) {
    // If the data set received is flagged as our format which contains the ID as the key
    if(!this.autocomplete_with_id) {
      // This is the first time this autocomplete object is receiving an enhanced version of our autocomplete
      // Flag that this autocomplete field ajax feed is using the autocomplete_with_id feature.
      this.autocomplete_with_id = true;
    }
    // Remove the extra field from the search data.
    delete matches["#autocomplete_with_id"];
    // Process the data received into a format the normal autocomplete expects
    var m = new Object();
    for (key in matches) {
      // We use the 'list' field for displaying in the list.
      m[key] = matches[key]["list"];
    }
    // Call the original 'found' method to give it a chance to do it's stuff.
    this.foundOrig(m);
    // Update the LI elements the original found method provided to include our ID and correct value.
    $(this.popup).find('li').each( function(index,element) {
      if($(element).data('autocompleteValue')) {
        var key = $(element).data('autocompleteValue');
        $(element).data('autocompleteID',key);
        $(element).data('autocompleteValue',matches[key]["input"]);
      }
    });
  } else if(this.idfield != undefined) {
    // We have an id field configured for storing autocomplete data, and the data format received is for the old style handler.
    // So let's try and use it by extracting the IDs in the old way and use them with our autocomplete handler.

    // matches is already in a format that the original 'found' handler accepts.
    this.foundOrig(matches);
    // Now process the popup results and store the enhanced data
    var ac = this;
    $(this.popup).find('li').each( function(index,element) {
      v = $(element).data('autocompleteValue');
      if(v) {
        m = ac.extractID(v);
        if(m!=false) {
          // A match was found, set our data values for ID and value.
          $(element).data('autocompleteID',m[0]);
          $(element).data('autocompleteValue',m[1]);
        } else {
          // Bad news, ID couldn't be extracted. We should probably let the user know.
        }
      }
    });
    
  } else {
    // This isn't using our enhanced method, call the original handler.
    this.foundOrig(matches);
  }
};

// Extract a value into it's ID and input/display values
Drupal.jsAC.prototype.extractID = function(value) {
  // Extract from the key (from end of string) the record id prefixed with nid/uid/?id separated with a colon : and enclosed in [] or ().
  // The regex to use for matched. Perhaps enhance to allow others to be configured through Drupal.settings.autocomplete.regex in the future.
  extract = /\s*[\[\(]([a-z]*id:)?([0-9]+)[\]\)]$/i;
  // The regex matches 2 things, firstly everything which is to be removed from the 'value', and secondly the ID itself.
  m = value.match(extract);
  if(m!=null && m.length==3 && m[2].length>0) {
    // We should match three things (the first is the whole of the regex matched, which is what we will strip off the value)
    //   The record prefix is the second item. The record ID is the third item. (The first item is what we will strip from the end of the string)
    id = m[2];
    // Extract the value from the start of the string
    v = value.substr(0, value.length-m[0].length );
    return [id,v];
  }
  // We couldn't find a match, which is bad news as we have no idea what the record id for this item is.
  return false;
};

// Revert back to the previous value so that the input matches the ID value stored.
Drupal.jsAC.prototype.revertValue = function() {
  if(this.input.value.length) {
    var v = $(this.idfield).data('inputValue');
    if(v == undefined) {
      // The user hasn't yet selected a new value for this field, so return to defaults.
      this.revertDefault();
      return;
    }
    this.input.value = v;
  } else {
    // Input is blank, so remove the ID.
    $(this.idfield).val('');
    // And we also update the input value (so that we don't revert back to the previous inputValue next time.)
    $(this.idfield).data('inputValue','')
  }
};

// Handler to revert to the form default values
Drupal.jsAC.prototype.revertDefault = function (input, e) {
  // Set the input field back to it's default
  this.input.value = $(this.input).attr('default-value');
  // Set the id field back to it's default
  $(this.idfield).val( $(this.idfield).attr('default-value') );
  // Update the input value stored on the id field so they match
  $(this.idfield).data('inputValue',this.input.value);
};

// Handlers to revert to the default value when the use hits escape.
Drupal.jsAC.prototype.revertonkeyup = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 27: // Esc.
      this.revertDefault();
  }
};

// Set up the ID field linked to the autocomplete field.
Drupal.jsAC.prototype.setupIdField = function() {
  // Locate our idfield
  fieldname = $(this.input).attr('for-field-id');
  // If the field does not have a 'for-field-id' attribute then we don't handle this field.
  if(fieldname == undefined || fieldname.length==0) {
    return;
  }
  // Escape the fieldname before we try and use it as a selector (characters listed are defined in jquery selectors api docs)
  //   See: http://api.jquery.com/category/selectors/
  escape = /([!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~])/g;
  var f = $('#' + fieldname.replace(escape, '\\$1') )[0];
  // If the field exists, then we setup the rest of our handlers.
  if(f != undefined) {
    this.idfield = f;
    // Set our default input value which matches this id field value
    // (Note: we use the default value as it's already changed by the point setup is called)
    //$(this.idfield).val('inputValue', $(this.input).attr('default-value') );
    // Setup our revert handler when the user clicks away
    var ac = this;
    $(this.input).blur(function() { ac.revertValue(); } );
    $(this.input).keyup(function() { ac.revertonkeyup(this, event); } );
  }
};

})(jQuery);


// Updates to run on document ready
jQuery(document).ready(function($) {
  // Hide the message for non-javascript users.
  $('div.form-autocomplete-nojavascript').hide();
  // Enable the autocomplete fields
  $('input.form-autocomplete').removeAttr('disabled');
});
