
Autocomplete using a textfield for the user to search for the record they want to select, without exposing
the ID of the item to the user.

Provides an autocomplete FAPI element, basically an enhanced version of a textfield with autocomplete, with
the benefit of hiding away the real record ID. It allows the user to search and select an appropriate record
as normal but uses a hidden field to store the selected record ID.


Description:
-----
This module provides an autocomplete FAPI element which records the ID of the item selected during autocompletion
so that it can be used during form submission to store the actual ID of the item.

This is in contrast to a normal textfield with autocomplete which simply stores the name of the autocomplete
item and doesn't have the ID available unless it's stamped directly in the text shown to the user.


Usability:
-----
Autocomplete only works when javascript is enabled, so we show a message that the field can't be completed
for non-javascript users.

The user can hit the escape key to return the field to the original value when the form was loaded.

For non-javascript users, they will not be able to edit the field as there is no way to serve them the
autocomplete list. They will be presented a disable field. It is suggested you don't make auto-complete
fields required fields if you want the form to be accessible to non-js users.
It may be possible to enhance the experience for non-js users by allowing them to interact with the field
using a search button which triggers a round-trip to the server and a form rebuilt to present available
options for them to select, however this is left as a future enhancement should the need actually arise.


Implementation:
-----
We are hooking into the existing autocomplete functionality whilst ensuring that all parts are still
executed as normal. The data structure returned by the autocomplete_path callback has a modified data
structure format. Our extensions should sit on top of and play nicely with the existing autocomplete
core functionality. Though naturally if the core functionality changes drastically then we might need
some tweaks.


Possible shortcomings:
-----
Which may or may not there, the author just hasn't tested them, are:
  Multiple items:
      The field only supports a single item being entered. (This isn't a tagging field!)
      The particular application was to replace very long 'select' option lists.

  No editing capabilities for non-javascript users:
      They are informed the field can only be edited using javascript, and the field is disabled for input.
      The value remains at it's original value when the form was presented to the user.
